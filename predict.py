import joblib
import json
import sys

def main():

  loaded_vectorizer = joblib.load("./models/vectorizer.pkl")
  loaded_category_classifier = joblib.load("./models/category_classifier.pkl")
  loaded_topic_classifier = joblib.load("./models/topic_classifier.pkl")

  if (len(sys.argv) != 2):
    sys.exit("Usage: %s \"QUESTION\"" % sys.argv[0])

  question = sys.argv[1]
  q = Question(question)
  q.predict_category(loaded_category_classifier, loaded_vectorizer)
  q.predict_topic(loaded_topic_classifier, loaded_vectorizer)

  data = {
      "topic": q.topic,
      "topic_probability": q.topic_probability,
      "categories": q.category
  }

  print(json.dumps(data))
  sys.stdout.flush()

class Sentiment:
  POSITIVE = 'YES'
  NEGATIVE = 'NO'

class Question:
  def __init__(self, text, category="", topic=""):
    self.text = text.lower()
    self.category = category.split(",")
    self.topic = topic.lower()

  def is_category(self, category):
    if (category in self.category):
      return Sentiment.POSITIVE
    else:
      return Sentiment.NEGATIVE

  def predict_category(self, models, vectorizer):
    x = vectorizer.transform([self.text])
    self.category = [category for category in list(models.keys()) if models[category].predict(x.toarray())[0] == Sentiment.POSITIVE]

  def predict_topic(self, model, vectorizer):
    x = vectorizer.transform([self.text])
    self.topic_probability = max(model.predict_proba(x.toarray())[0])
    self.topic = model.predict(x.toarray())[0]

if __name__ == "__main__":
  main()
