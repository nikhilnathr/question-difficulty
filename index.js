
const express = require('express')
const app = express()
const port = process.env.PORT || 3000;

app.get('/', (req, res) => {
  const question = req.query.q;

  if (!question) {
    res.send({ "message": "No question given" });
    return;
  }

  const spawn = require("child_process").spawn;
  const pythonProcess = spawn('python3', ["./predict.py", question]);

  pythonProcess.stdout.on('data', (data) => {
    res.send(JSON.parse(data.toString()));
  });
})

app.listen(port, () => {
  console.log(`App listening at http://localhost:${port}`);
});

